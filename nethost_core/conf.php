<?php

final class Conf
{

    protected static $_instance;

    /**
     *
     * @return Conf
     */
    public static function getInstance()
    {
        if (! isset(self::$instance)) {
            $c = __CLASS__;
            self::$_instance = new $c();
        }
        
        return self::$_instance;
    }

    public $enableHttps = false;

    public $sysName = "SÜSTEEMI NIMI";

    public $isBeta = true;    
    
    // debug
    public $debug = true;

    // DB INFO
    public $useDB = true;

    public $dbHost = "localhost";

    public $dbPort = "3307";

    public $dbUser = "root";

    public $dbPassword = "Tcr5t6";

    public $dbDatabase = "nh_demo";

    public $tablePrefix = "nh_";

    // teisendatakse kujule aaa/aaa/
    public $subpages = array();

    // auto
    public $autoLoaderClass = 'Autoloader';

    // autoloader functions
    public $autoLoaderFunctions = array(
        "ApplicationLoader",
        "ModelLoader",
        "PluginLoader",
        "CronLoader",
        "WebsocketsLoader"
    );

    // default muutujad
    public $defaultWebpage = 'nethost';

    public $defaultController = 'Index';

    public $defaultMethod = 'index';

    public $customRequired = array(
        'webpages/main/Controller/Main/Main.class.php',
        'webpages/nethost/Controller/Nethost/Nethost.class.php'
    );

    public $languages = array(
        'ee',
        'en',
        'ru'
    );

    public $defaultLanguage = 'ee';

    public $enableLog = true;

    public $logColor = "#000000";

    public $criticalQueryTime = 0.05;

    // ********* DO NOT EDIT AFTER THIS ***********//
    public $tableMarker = '__PREFIX__';
}
?>