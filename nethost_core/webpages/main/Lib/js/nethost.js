nethost = {};
nethost.data = {};
nethost.form = {};
nethost.swal = {};
nethost.alert = {};
nethost.alert.stack = {};
nethost.ajax = {};

nethost.alert.stack.stack_top_left = {"dir1": "down", "dir2": "right", "push": "top"};
nethost.alert.stack.stack_bottom_left = {"dir1": "right", "dir2": "up", "push": "top"};
nethost.alert.stack.stack_bottom_right = {"dir1": "up", "dir2": "left", "firstpos1": 25, "firstpos2": 25};

//data on jsoni formaadis STRING!
nethost.ajax.post = function(url, data = "{}", successCallback = null, errorCallback = null) {	
	$.ajax({
         type: "POST",
         url: url,
         data: JSON.parse(data),
         success: function (data) {  
             var result = JSON.parse(data);
             var success = result.success;
             
             if(success == true) {
            	 if (typeof successCallback === "function") {
            		successCallback(result);
            		return;
            	 }            	        	 
             } else {
            	 //andmete salvestamisel juhtus viga
            	 if (typeof errorCallback === "function") {
            		errorCallback(result);
             		return;
             	 }
            	 nethost.swal.fail("Viga", result.data.error);            	 
             }      
             
         },
         error: function (data) { 
             var result = JSON.parse(data);
             
             nethost.swal.fail("Viga", "Tundmatu viga");
         }
     }); 
	 return false;
}

/*
 * Styles:
 * 			primary
 * 			danger
 * 			success
 * 			warning
 * 			info
 * 			
 */
nethost.alert.notify = function(title, text, style, stack="top-right"){ 
	var stackSelected = null;
	
	switch(stack){
		case "top-right":	
			new PNotify({
		        title: title,
		        text: text,
		        addclass: 'bg-' + style
		    });  
			return;
			break;
		case "top-left":			
			stackSelected = nethost.alert.stack.stack_top_left;
			new PNotify({
		        title: title,
		        text: text,
		        addclass: 'stack-top-left bg-' + style,
		        stack: stackSelected
		    }); 
			return;
			break;
		case "bottom-left":			
			stackSelected = nethost.alert.stack.stack_bottom_left;
			new PNotify({
		        title: title,
		        text: text,
		        addclass: 'stack-bottom-left bg-' + style,
		        stack: stackSelected
		    }); 
			return;
			break;
		case "bottom-right":			
			stackSelected = nethost.alert.stack.stack_bottom_right;
			new PNotify({
		        title: title,
		        text: text,
		        addclass: 'stack-bottom-right bg-' + style,
		        stack: stackSelected
		    }); 
			return;
			break;
	}	
}

nethost.data.delete = function(deleteUrl, objId, showConfirm = true, callback = null){
	if(showConfirm == true){
		nethost.swal.confirm(
				"Oled kindel?", 
				"Andmed kustutatakse", 
				confirmButtonText = "Kustuta",
				cancelButtonText = "Loobu", 
				function(){
					if (typeof callback === "function") {
						callback();
					} else {
						window.location.href = deleteUrl + '/' + objId;
					}
				});
	} else {
		if (typeof callback === "function") {
			callback();
		} else {
			window.location.href = deleteUrl + '/' + objId;
		}
	}
}

nethost.form.post = function(form, enableConfirm = false, successCallback = null, errorCallback = null) {
		
	$.ajax({
         type: $(form).attr('method'),
         url: $(form).attr('action'),
         data: new FormData(form),
         processData: false,  // tell jQuery not to process the data
         contentType: false,  // tell jQuery not to set contentType
         success: function (data) {  
             var result = JSON.parse(data);
             var success = result.success;
             
             if(success == true) {
            	 if (typeof successCallback === "function") {
            		successCallback(result);
            		return;
            	 }
            	 
            	 //andmed salvestatud, kõik ok
            	 if(enableConfirm == true){
            		nethost.swal.success("Andmed salvestatud", "", function(){window.location.href = result.data.backUrl});
            	 } else {
            		window.location.href = result.data.backUrl;
            	 }            	 
             } else {
            	 //andmete salvestamisel juhtus viga
            	 if (typeof errorCallback === "function") {
            		errorCallback(result);
             		return;
             	 }
            	 nethost.swal.fail("Viga", result.data.error);
            	 
             }         
             
         },
         error: function (data) { 
             var result = JSON.parse(data);
             
             nethost.swal.fail("Viga", "Tundmatu viga");
         }
     });
	 
	 return false;
}

nethost.swal.success = function (title, text, callback = null) {
	swal({
			title: title,
			text: text,
			type: 'success',
			showCancelButton: false,
			confirmButtonColor: '#3085d6',
			confirmButtonText: 'OK'
		}).then(function () {
			if (typeof callback === "function") {
				callback();
			}
		})
}

nethost.swal.fail = function (title, text, callback = null) {
	swal({
			title: title,
			text: text,
			type: 'error',
			showCancelButton: false,
			confirmButtonColor: '#3085d6',
			confirmButtonText: 'OK'
		}).then(function () {
			if (typeof callback === "function") {
				callback();
			}
		})
}

nethost.swal.confirm = function (title, text, confirmButtonText = "Kustuta", cancelButtonText = "Loobu", callback = null) {
	swal({
		  title: title,
		  text: text,
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#d33',
		  cancelButtonColor: '#3085d6',
		  confirmButtonText: confirmButtonText,
		  cancelButtonText: cancelButtonText
		}).then(function () {
			if (typeof callback === "function") {
				callback();
			}
		}, function (dismiss) {
		  if (dismiss === 'cancel') {
		    
		  }
		});
}