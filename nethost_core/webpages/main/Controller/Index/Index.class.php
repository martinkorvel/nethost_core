<?php
class IndexController extends MainController{
	public function __construct(){
		$this->name = "Index";
		parent::__construct();
	}
	
	public function index(){ 
	    return $this->show("Index/index.html");
	}	
}
?>