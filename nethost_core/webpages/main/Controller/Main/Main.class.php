<?php

class MainController extends ControllerAPI
{

    public function __construct()
    {
        global $CURRENTWEB;

        $CURRENTWEB = lcfirst("main");
        parent::__construct();
    }

    public function show($template)
    {
        return $this->fetch($template);
    }

    public function consoleComponent($logText)
    {
        $this->S->assign("log_text", $logText);

        if (Conf::getInstance()->enableLog) {
            return $this->fetch("Components/console.html");
        }
    }
}
?>