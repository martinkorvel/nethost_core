<?php
class LoginController extends NethostController{
	public function __construct(){
		$this->name = "Login";
		parent::__construct();
	}
	
	public function index() { 
	    $link = array();
	    
	    $link['postUrl'] = Link::getInstance()->createUrl($this->name, "doLogin");
	    
	    $this->S->assign('link', $link);
	    
	    return $this->show("Login/index.html");
	}	
	
	public function doLogin() {
	    global $VARS;
	    
	    $login = new Login();
	    
	    $login->setUsername($VARS->get('username'));
	    $login->setPassword($VARS->get('password'));
	    
	    $login->doLogin();
	    
	    Util::go(Link::getInstance()->createFullUrl("nethost"));
	}
}
?>