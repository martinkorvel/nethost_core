<?php
class IndexController extends NethostController{
	public function __construct(){
		$this->name = "Index";
		parent::__construct();
		
		if(!Login::getInstance()->isUserLogged()) {
		    Util::go(Link::getInstance()->createUrl("Login", "Index"));
		}
	}
	
	public function index(){ 
	    return $this->show("Index/index.html");    
	}	
}
?>