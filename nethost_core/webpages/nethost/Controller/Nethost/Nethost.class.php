<?php

class NethostController extends ControllerAPI
{

    public function __construct()
    {
        global $CURRENTWEB;

        $CURRENTWEB = lcfirst("nethost");
        parent::__construct();
    }

    public function show($template)
    {
        return $this->fetch($template);
    }
}
?>