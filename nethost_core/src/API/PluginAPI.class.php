<?php

abstract class PluginAPI extends ClassAPI
{

    public $DB;

    public function __construct()
    {
        global $DB;

        $this->DB = $DB;
        parent::__construct();
    }
}
?>