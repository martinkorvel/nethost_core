<?php

abstract class DatabaseAPI extends ClassAPI
{

    public $_connection;

    // Need kirjutab parent üle. St peab kirjutama
    public function _connect()
    { /* dummy */
    }

    public function _disconnect()
    { /* dummy */
    }

    public function _dbSql($sql)
    { /* dummy */
    }

    // SELLE PEAB KA ÜLE KIRJUTAMA!!!
    public function query($str, $showErrors = false)
    {}

    public function insert($table, $data)
    {
        $sql = "INSERT INTO $table(";
        foreach ($data as $i => $k) {
            $sql .= "`" . $i . "`" . ", ";
        }
        $sql = substr($sql, 0, - 2);
        $sql .= ") VALUES(";
        foreach ($data as $i => $k) {
            $sql .= "'" . $k . "'" . ", ";
        }
        $sql = substr($sql, 0, - 2);
        $sql .= ")";

        return $this->_dbSql($sql);
    }

    /*
     * return null
     */
    public function update($table, $data, $condition)
    {
        $sql = "UPDATE $table SET ";
        foreach ($data as $i => $k) {
            $sql .= "`" . $i . "`='" . $k . "', ";
        }
        $sql = substr($sql, 0, - 2);
        $sql .= " WHERE $condition";
        $this->_dbSql($sql);

        return true;
    }

    public function delete($table, $where = "")
    {
        $sql = "DELETE FROM `$table`";
        if ($where) {
            $sql .= " where $where";
        }
        $this->_dbSql($sql);

        return true;
    }

    public function _prepSQL($sql)
    {
        $tablePrefix = Conf::getInstance()->tablePrefix;
        $tableMarker = Conf::getInstance()->tableMarker;

        return str_replace($tableMarker, $tablePrefix, $sql);
    }
}
?>