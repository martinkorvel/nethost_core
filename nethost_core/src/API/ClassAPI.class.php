<?php

/**
 * 
 * @author Martin
 *
 */
abstract class ClassAPI extends MainAPI
{

    public static function getLogTable()
    {
        return Conf::getInstance()->tablePrefix . "sys_log";
    }

    public function translate($phrase)
    {
        global $TRANSLATOR;
        if (Conf::getInstance()->useDB) {
            return $TRANSLATOR->translate($phrase);
        }
    }

    public function sys_log($what, $method, $args)
    {
        global $DB;

        $ins = array();

        if (class_exists("User")) {
            $uid = (int) User::getInstance()->getCurrentId();
            if ($uid) {
                $ins['user'] = $uid;
            }
        }

        $ins['action'] = $what ? addslashes($what) : '';
        $ins['class'] = get_class($this);
        $ins['method'] = $method;
        $ins['args'] = $args ? addslashes($args) : '';

        $DB->insert(self::getLogTable(), $ins);
    }
}
?>