<?php

abstract class RestAPI extends ClassAPI
{

    public $DB;

    public function __construct()
    {
        global $DB;

        $this->DB = $DB;
        parent::__construct();
    }

    public static function success($array)
    {
        return array(
            'success' => true,
            'data' => $array
        );
    }

    public static function fail($array)
    {
        return array(
            'success' => false,
            'data' => $array
        );
    }
}
?>