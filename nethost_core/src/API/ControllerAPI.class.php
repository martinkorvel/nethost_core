<?php

/**
 * 
 * @author Martin
 *
 */
abstract class ControllerAPI extends ClassAPI
{

    public $S;

    public $name;

    const WEBPAGES_DIR = 'webpages/';

    const LIB_DIR = '/Lib/';

    const VIEW_DIR = '/View/';

    public function __construct()
    {
        global $S;

        $this->S = $S;
        parent::__construct();
    }

    public function fetch($template)
    {
        global $CURRENTWEB;

        $this->S->setTemplateDir(SITE_PATH . self::WEBPAGES_DIR . $CURRENTWEB . self::VIEW_DIR);
        $this->S->assign('libDir', SERVER_NAME . self::WEBPAGES_DIR . $CURRENTWEB . self::LIB_DIR);
        $this->S->assign('this', $this);
        $this->S->assign('sysName', Conf::getInstance()->sysName);
        $this->S->assign('isBeta', Conf::getInstance()->isBeta);

        return $this->S->fetch($template);
    }

    public function fetchJavascript($template)
    {
        global $CURRENTWEB;

        $this->S->setTemplateDir(SITE_PATH . self::WEBPAGES_DIR . $CURRENTWEB . self::LIB_DIR);
        $this->S->assign('libDir', SERVER_NAME . self::WEBPAGES_DIR . $CURRENTWEB . self::LIB_DIR);
        $this->S->assign('this', $this);
        $this->S->assign('sysName', Conf::getInstance()->sysName);
        $this->S->assign('isBeta', Conf::getInstance()->isBeta);

        return $this->S->fetch($template);
    }

    public function jsonError($error)
    {
        $params = array();

        $params['success'] = false;
        $params['data'] = array(
            'error' => $error
        );

        echo json_encode($params);
        exit();
    }

    /**
     *
     * @param array $data
     */
    public function jsonSuccess($data = array())
    {
        if (! array_key_exists("msg", $data)) {
            $data['msg'] = $this->translate('Andmed salvestatud');
        }

        $params = array();

        $params['success'] = true;
        $params['data'] = $data;

        echo json_encode($params);
        exit();
    }

    public function jsonWrite($params)
    {
        echo json_encode($params);
        exit();
    }
}
?>