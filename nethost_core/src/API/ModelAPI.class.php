<?php

abstract class ModelAPI extends ClassAPI
{

    public $DB;

    public function __construct()
    {
        global $DB;

        $this->DB = $DB;
        parent::__construct();
    }
}
?>