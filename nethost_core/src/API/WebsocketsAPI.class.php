<?php

abstract class WebsocketsAPI extends WebSocketServer
{

    public $DB;

    public function __construct($host, $port)
    {
        global $DB;

        $this->DB = $DB;
        parent::__construct($host, $port);
    }
}
?>