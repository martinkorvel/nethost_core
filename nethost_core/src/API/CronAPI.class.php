<?php

abstract class CronAPI extends ModelAPI implements Icron
{

    public $DB;

    private $pattern;

    const EVERY_SECOND = 1;

    const EVERY_MINUTE = 2;

    const EVERY_HOUR = 3;

    const EVERY_DAY = 4;

    const EVERY_WEEK = 5;

    const EVERY_MONTH = 6;

    const EVERY_YEAR = 7;

    const DATE_FORMAT = "Y-m-d H:i:s";

    private static function getTableName()
    {
        return Conf::getInstance()->tablePrefix . "cron";
    }

    private static function getLogTableName()
    {
        return Conf::getInstance()->tablePrefix . "cron_log";
    }

    public function __construct($pattern)
    {
        global $DB;

        $this->DB = $DB;
        parent::__construct();

        $this->setPattern($pattern);
        $this->registerCron();
    }

    public function setPattern($pattern)
    {
        $this->pattern = (int) $pattern;
    }

    public function getPattern()
    {
        return $this->pattern;
    }

    private function registerCron()
    {
        $class = get_class($this);

        $result = $this->DB->query("SELECT id FROM " . $this->getTableName() . " WHERE class = '$class'");

        if ($result) {
            $this->DB->update($this->getTableName(), array(
                'pattern' => $this->getPattern()
            ), "id = " . (int) $result[0]['id']);
        } else {
            $this->DB->insert($this->getTableName(), array(
                'class' => $class,
                'pattern' => $this->getPattern()
            ));
        }
    }

    public function execute()
    {
        $class = get_class($this);

        $sql = "SELECT * FROM " . self::getLogTableName() . " WHERE cron_id = (SELECT id FROM " . self::getTableName() . " WHERE class = '" . $class . "') ORDER BY id DESC LIMIT 1";

        $result = $this->DB->query($sql);

        if (! $result) {
            return $this->saveLog($class, $this->getPattern());
        } else {
            $nextRun = $result[0]['next_run'];
            $currDate = date(self::DATE_FORMAT);

            if ($nextRun <= $currDate) {
                return $this->saveLog($class, $this->getPattern());
            }
        }

        return false;
    }

    public function saveLog($class, $pattern)
    {
        $idSql = "SELECT id FROM " . self::getTableName() . " WHERE class = '" . $class . "'";

        $res = $this->DB->query($idSql);

        if ($res) {
            $id = (int) $res[0]['id'];
            $lastRun = date(self::DATE_FORMAT);

            switch ($pattern) {
                case self::EVERY_SECOND:
                    $nextRun = date(self::DATE_FORMAT, strtotime("+1 seconds"));
                    break;
                case self::EVERY_MINUTE:
                    $nextRun = date(self::DATE_FORMAT, strtotime("+1 minutes"));
                    break;
                case self::EVERY_HOUR:
                    $nextRun = date(self::DATE_FORMAT, strtotime("+1 hours"));
                    break;
                case self::EVERY_DAY:
                    $nextRun = date(self::DATE_FORMAT, strtotime("+1 days"));
                    break;
                case self::EVERY_MONTH:
                    $nextRun = date(self::DATE_FORMAT, strtotime("+1 months"));
                    break;
                case self::EVERY_YEAR:
                    $nextRun = date(self::DATE_FORMAT, strtotime("+1 years"));
                    break;
                default:
                    break;
            }

            $ins = array();

            $ins['cron_id'] = $id;
            $ins['last_run'] = $lastRun;
            $ins['next_run'] = $nextRun;

            $this->DB->insert(self::getLogTableName(), $ins);
        }

        return false;
    }
}
?>