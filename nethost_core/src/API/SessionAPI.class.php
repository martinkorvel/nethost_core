<?php

class SessionAPI extends MainAPI
{

    public function __construct()
    {}

    public function get($k)
    {
        if (array_key_exists($k, $_SESSION)) {
            return $_SESSION[$k];
        }

        return null;
    }

    public function set($k, $i)
    {
        $_SESSION[$k] = $i;
    }
}
?>