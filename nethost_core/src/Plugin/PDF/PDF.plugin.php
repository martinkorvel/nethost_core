<?php
/**
 * 
 * @author Martin
 *
 */
require_once(SITE_PATH . 'lib/tcpdf/config/tcpdf_config.php');
require_once(SITE_PATH . 'lib/tcpdf/tcpdf.php');
class PDFPlugin extends PluginAPI {
    private $_author;
    private $_title;
    private $_subject;
    private $_content;
	
	public function setData($author, $title, $subject, $content){
	    $this->_author = $author;
	    $this->_title = $title;
	    $this->_subject = $subject;
	    $this->_content = $content;
	}
	
	public function export(){
	    set_time_limit (0);
	    error_reporting(0);
	    // create new PDF document
	    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	    // set document information
	    $pdf->SetCreator(PDF_CREATOR);
	    $pdf->SetAuthor($this->_author);
	    $pdf->SetTitle($this->_title);
	    $pdf->SetSubject($this->_subject);
	    $pdf->setPrintFooter(false);
	    $pdf->setPrintHeader(false);
	    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
	    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	    
	    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	    
	    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	    
	    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	    
	    $pdf->SetFont('times', '', 10);
	    
	    $pdf->AddPage();
	    
	    $html = $this->_content;
	    
	    // output the HTML content
	    $pdf->writeHTML($html, true, false, true, false, '');
	
	    $pdf->Output($this->_title . '.pdf', 'I');
	}
}
