<?php
/**
 * 
 * @author Martin
 *
 */
class DefaultCron extends CronAPI {
	public function __construct() {
		parent::__construct(self::EVERY_MINUTE);		
	}	
	
	public function execute(){
		if(parent::execute()){
			//LOGIC HERE
		}
	}
}
