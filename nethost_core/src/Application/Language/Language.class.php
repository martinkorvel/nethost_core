<?php
final class Language extends ClassAPI{
	private static $_instance;

	public static function getInstance(){
		if(!self::$_instance instanceof self){
			self::$_instance = new Language();
		}
		
		return self::$_instance;
	}	

	public function getCurrentLanguage(){
		if(Session::getInstance()->get('lang')){
			return Session::getInstance()->get('lang'); 
		}	
		
		return Conf::getInstance()->defaultLanguage;
	}
	
	public function setCurrentLanguage($to){
	    if(in_array($to, Conf::getInstance()->languages)) {
	        Session::getInstance()->set('lang', $to);
	    } else {
	        $this->resetLanguage();
	    }		
	}
	
	public function resetLanguage() {
	    Session::getInstance()->set('lang', Conf::getInstance()->defaultLanguage);
	}
}