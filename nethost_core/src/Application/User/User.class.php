<?php

class User extends ClassAPI
{

    private $_id;

    private $_email;

    private $_username;

    private $_password;

    private $_firstName;

    private $_lastName;

    private $_created;

    private $_modified;

    public function setId($value)
    {
        $this->_id = $value;
    }

    public function getId()
    {
        return $this->_id;
    }

    public function setEmail($value)
    {
        $this->_email = $value;
    }

    public function getEmail()
    {
        return $this->_email;
    }

    public function setUsername($value)
    {
        $this->_username = $value;
    }

    public function getUsername()
    {
        return $this->_username;
    }

    public function setPassword($value)
    {
        $this->_password = $value;
    }

    public function getPassword()
    {
        return $this->_password;
    }

    public function setFirstName($value)
    {
        $this->_firstName = $value;
    }

    public function getFirstName()
    {
        return $this->_firstName;
    }

    public function setLastName($value)
    {
        $this->_lastName = $value;
    }

    public function getLastName()
    {
        return $this->_lastName;
    }

    public function setCreated($value)
    {
        $this->_created = $value;
    }

    public function getCreated()
    {
        return $this->_created;
    }

    public function setModified($value)
    {
        $this->_modified = $value;
    }

    public function getModified()
    {
        return $this->_modified;
    }

    public function __construct($uid = null)
    {
        global $DB;

        $uid = (int) $uid;

        if ($uid) {
            $qry = "SELECT * FROM " . self::getTableName() . " where id = $uid";

            $res = $DB->query($qry);

            if ($res) {
                $res = $res[0];
                $this->setId($res['id']);
                $this->setUsername($res['username']);
                $this->setPassword($res['password']);
                $this->setFirstName($res['first_name']);
                $this->setLastName($res['last_name']);
                $this->setCreated($res['created']);
                $this->setModified($res['modified']);
                $this->setEmail($res['email']);
            }
        }

        parent::__construct();
    }

    public static function getTableName()
    {
        return Conf::getInstance()->tablePrefix . "user";
    }

    public static function getUser($uid = null)
    {
        if (! $uid) {
            $uid = (int) Session::getInstance()->get('uid');
        }
        if (! $uid) {
            return new User();
        }

        return new User($uid);
    }

    public function getCurrentId()
    {
        $uid = (int) Session::getInstance()->get('uid');

        return (int) $uid;
    }
}
?>