<?php

final class InstallerClient extends ClassAPI
{

    private static $_instance;

    public static function getInstance()
    {
        if (! self::$_instance instanceof self) {
            self::$_instance = new InstallerClient();
        }

        return self::$_instance;
    }

    private $_table = "";

    public function __construct()
    {
        parent::__construct();
        $this->_table = Conf::getInstance()->tablePrefix . 'version_client';
    }

    public function getInstalledVersion($asSql = false)
    {
        global $DB;

        $version = $DB->query("SELECT * FROM {$this->_table} order by id desc limit 1", false);

        if ($version) {
            $versionData = $version[0];
            $version = $versionData[Installer::STRING_VERSION];
            $major = $versionData[Installer::STRING_MAJOR];
            $minor = $versionData[Installer::STRING_MINOR];
            $revision = $versionData[Installer::STRING_REVISION];
            if ($asSql) {
                return $version . "." . $major . "." . $minor . "." . $revision . ".sql";
            } else {
                return $version . "." . $major . "." . $minor . "." . $revision;
            }
        }

        return 0;
    }

    private function _checkIfVersionInstalled($version)
    {
        global $DB;

        $parts = explode(".", $version);

        $ins = array();

        $ins[Installer::STRING_VERSION] = $parts[0];
        $ins[Installer::STRING_MAJOR] = $parts[1];
        $ins[Installer::STRING_MINOR] = $parts[2];
        $ins[Installer::STRING_REVISION] = $parts[3];

        $res = $DB->query("SELECT 1 FROM {$this->_table} where version = {$ins[Installer::STRING_VERSION]} and major = {$ins[Installer::STRING_MAJOR]} and minor = {$ins[Installer::STRING_MINOR]} and revision = {$ins[Installer::STRING_REVISION]}", false);
        if ($res) {
            return true;
        }

        return false;
    }

    public function update()
    {
        global $DB;

        $versionPure = $this->getInstalledVersion();
        $version = $versionPure . ".sql";

        $path = SITE_PATH . 'upgrade/client/SQL/';

        $return = "";

        $files = scandir($path);
        unset($files[0]);
        unset($files[1]);

        if (! $version) {
            foreach ($files as $file) {
                if (is_readable($path . $file)) {
                    $handle = fopen($path . $file, "r");
                    if ($handle) {
                        while (($line = fgets($handle)) !== false) {
                            $DB->_dbSql($line);
                        }

                        fclose($handle);
                    } else {
                        // error opening the file.
                    }
                    $return .= "CLIENT SQL installed " . $file . PHP_EOL;
                    $parts = explode(".", $file);

                    $ins = array();

                    $ins[Installer::STRING_VERSION] = $parts[0];
                    $ins[Installer::STRING_MAJOR] = $parts[1];
                    $ins[Installer::STRING_MINOR] = $parts[2];
                    $ins[Installer::STRING_REVISION] = $parts[3];
                    $DB->insert($this->_table, $ins);
                }
            }
        } else {
            foreach ($files as $file) {
                if (is_readable($path . $file) && version_compare($versionPure, str_replace(".sql", "", $file), "<") && ! $this->_checkIfVersionInstalled($file)) {
                    $handle = fopen($path . $file, "r");
                    if ($handle) {
                        while (($line = fgets($handle)) !== false) {
                            $DB->_dbSql($line);
                        }

                        fclose($handle);
                    } else {
                        // error opening the file.
                    }
                    $return .= "CLIENT SQL installed " . $file . PHP_EOL;
                    $parts = explode(".", $file);
                    $ins[Installer::STRING_VERSION] = $parts[0];
                    $ins[Installer::STRING_MAJOR] = $parts[1];
                    $ins[Installer::STRING_MINOR] = $parts[2];
                    $ins[Installer::STRING_REVISION] = $parts[3];
                    $DB->insert($this->_table, $ins);
                }
            }
        }

        return $return;
    }
}