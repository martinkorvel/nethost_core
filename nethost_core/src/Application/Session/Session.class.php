<?php
class Session extends SessionAPI{
	public function __construct(){
		parent::__construct();		
		if($_SESSION){
			foreach($_SESSION as $k=>$v){
				$this->_regisrty[$k] = $v;
			}
		}
	}

	protected static $_instance;
	/**
	 * 
	 * @return Session
	 */
	public static function getInstance() {
		if (! isset ( self::$instance )) {
			$c = __CLASS__;
			self::$_instance = new $c ();
		}

		return self::$_instance;
	}
}
?>