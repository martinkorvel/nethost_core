<?php
final class Screen extends ClassAPI{
	public function __construct(){		
		parent::__construct();
		$this->clearBuffer();
	}
	
	private $_buffer;
	
	public function clearBuffer(){		
	
	}
	
	public function add($data){
		$this->_buffer=$this->_buffer.$data;	
	}
	
	public function render(){		
		echo $this->_buffer;	
		$this->clearBuffer();
	}
	
}