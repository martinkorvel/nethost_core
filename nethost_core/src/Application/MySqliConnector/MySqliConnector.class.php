<?php

class MySqliConnector extends DatabaseAPI implements Idatabase
{

    private $_host;

    private $_user;

    private $_pass;

    private $_dbName;

    private $_port;

    public function __construct($host, $user, $pass, $dbName, $port)
    {
        parent::__construct();

        $this->_host = $host;
        $this->_user = $user;
        $this->_pass = $pass;
        $this->_dbName = $dbName;
        $this->_port = $port;

        try {
            $this->_connect();
            $this->_disconnect();
        } catch (Exception $ex) {
            throw new mysqli_sql_exception();
        }
    }

    public function _connect()
    {
        $this->_connection = mysqli_connect($this->_host, $this->_user, $this->_pass, $this->_dbName, $this->_port);
        if (! $this->_connection) {
            throw new mysqli_sql_exception();
        }

        mysqli_set_charset($this->_connection, "utf8");
    }

    public function _disconnect()
    {
        if ($this->_connection) {
            mysqli_commit($this->_connection);
            mysqli_close($this->_connection);
        }
    }

    public function _dbSql($sql)
    {
        global $LOG;

        $time = microtime(true);

        $this->_connect();
        mysqli_query($this->_connection, $this->_prepSQL($sql));
        $lastID = mysqli_insert_id($this->_connection);
        $error = mysqli_error($this->_connection);
        $this->_disconnect();
        if ($error) {
            throw new mysqli_sql_exception();
        }
        $LOG->append($sql, microtime(true) - $time);

        return $lastID;
    }

    public function query($qry, $showErrors = true)
    {
        global $LOG;

        $res = array();

        $time = microtime(true);

        $this->_connect();
        $result = mysqli_query($this->_connection, $qry);
        if ($result) {
            $count = mysqli_num_rows($result);
            if ($count > 0) {
                while ($r = mysqli_fetch_assoc($result)) {
                    $res[] = $r;
                }
                $this->_disconnect();

                $LOG->append($qry, microtime(true) - $time);

                return $res;
            }
        }
        $error = mysqli_error($this->_connection);
        $this->_disconnect();
        if ($showErrors && $error) {
            throw new mysqli_sql_exception();
        }

        $LOG->append($qry, microtime(true) - $time);

        return null;
    }

    public function checkIfDatabaseExist($database)
    {
        global $LOG;

        $time = microtime(true);

        $sql = "SHOW DATABASES LIKE '$database'";

        $result = $this->query($sql);

        $LOG->append($sql, microtime(true) - $time);

        if ($result) {
            return true;
        }

        return false;
    }

    public function checkIfTableExists($table)
    {
        global $LOG;

        $time = microtime(true);

        $res = $this->query("SELECT table_name FROM information_schema.tables WHERE table_schema = '{$this->_dbName}' AND table_name = '$table';");
        if ($res) {
            return true;
        }

        $LOG->append("SELECT table_name FROM information_schema.tables WHERE table_schema = '{$this->_dbName}' AND table_name = '$table';", microtime(true) - $time);

        return false;
    }
}
?>