<?php

class Login extends User
{

    protected static $_instance;

    /**
     *
     * @return Login
     */
    public static function getInstance()
    {
        if (! isset(self::$instance)) {
            $c = __CLASS__;
            self::$_instance = new $c();
        }

        return self::$_instance;
    }

    public function isUserLogged()
    {
        if (Session::getInstance()->get("uid")) {
            return true;
        }

        return false;
    }

    public function doLogin()
    {
        global $DB;

        $username = addslashes($this->getUsername());
        $password = Util::getPassword($this->getPassword());

        $qry = "SELECT * FROM " . self::getTableName() . " where username = '$username' and password = '$password'";
        
        $res = $DB->query($qry);

        if ($res) {
            $res = $res[0];
            Session::getInstance()->set("uid", $res['id']);
            return new User($res['id']);
        }

        return false;
    }

    public function doLoginIdLogin($idCode)
    {
        global $DB;

        $idCode = addslashes($idCode);

        $qry = "SELECT * FROM " . self::getTableName() . " where isikukood = '$idCode' and new_account = 0 LIMIT 1";

        $res = $DB->query($qry);

        if ($res) {
            $res = $res[0];
            Session::getInstance()->set("uid", $res['id']);
            return new User($res['id']);
        }

        return false;
    }

    public function doLogout()
    {
        session_destroy();
    }

    public function getCurrentId()
    {
        $uid = (int) Session::getInstance()->get('uid');

        return (int) $uid;
    }
}
?>