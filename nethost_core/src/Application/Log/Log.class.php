<?php
final class Log extends ClassAPI {
	private static $_instance;
	private $_log = array ();
	
	public static function getInstance() {
		if (! self::$_instance instanceof self) {
			self::$_instance = new Log ();
		}
		return self::$_instance;
	}
	
	public function __construct() {
		parent::__construct ();
		
		$this->append ( "<br>DEBUG LOG<br>");
		if(key_exists('REQUEST_URI', $_SERVER)){
			$this->append ( "URI: " . $_SERVER ['REQUEST_URI'] );
		}
		$this->append ( "POST: " . $this->printArray ( $_POST ) );
		$this->append ( "GET: " . $this->printArray ( $_GET ) );
		$this->append ( "FILES: " . $this->printArray ( $_FILES ) );
		$this->append ( "COOKIE: " . $this->printArray ( $_COOKIE ) );
		$this->append ( "SESS: " . $this->printArray ( $_SESSION ) );
	}
	
	public function write() {
	    if (Conf::getInstance()->enableLog) {
			$buffer = null;
			
			foreach ( $this->_log as $k => $i ) {
				if ($k > 0) {
					$buffer .= $k . " " . $i . PHP_EOL . '<br>';
				} else {
					$buffer .= $i . PHP_EOL . '<br>';
				}
			}
			
			if ($buffer) {
			    return '<font color="' . Conf::getInstance()->logColor . '">' . $buffer . '</font>';
			}
		}
	}
	
	public function append($message, $time = null) {
		$buffer = null;
		
		if ($time > Conf::getInstance()->criticalQueryTime) {
			$buffer = '<font color="red">' . $time . '</font>';
		} else if ($time) {
			$buffer = $time;
		}
		if ($buffer) {
			$this->_log [] = "Time: " . $buffer . " - " . $message;
		} else {
			$this->_log [] = $message;
		}
	}
	
	private function printArray($array) {
		if (is_array ( $array )) {
			$buffer = "(";
			
			$first = true;
			
			foreach ( $array as $k => $i ) {
				if ($first) {
					if (! is_array ( $i )) {
						$buffer .= '"' . $k . '"' . '=>' . '"' . $i . '"';
						
						$first = false;
					}
				} else {
					if (! is_array ( $i )) {
						$buffer .= ', "' . $k . '"' . '=>' . '"' . $i . '"';
					}
				}
			}
			
			return 'Count: ' . sizeof ( $array ) . ' ' . $buffer . ')';
		}
	}
}