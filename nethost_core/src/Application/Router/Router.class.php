<?php
/**
 *
 *
 *
 * @author Martin
 *
 */
class Router {
	public static function route(Request $request, $isAlias = false) {	   
	    $aliasName = $request->getWebpage();	    
	    $alias = Alias::getAliasByName($aliasName);
	    
	    if($alias) {
	        $request->setWebpage($alias->getWebpage());
	        $request->setController($alias->getController());
	        $request->setMethod($alias->getMethod());
	        $request->setArgs(unserialize($alias->getArgs()));
	    } 	    
	    
	    $webpage = $request->getWebpage () ? $request->getWebpage () : Conf::getInstance()->defaultWebpage;
		
	    if(!in_array($request->getTran (), Conf::getInstance()->languages)){
			$request->setTran(Language::getInstance()->getCurrentLanguage());
		}
		
		$tran = $request->getTran () ? $request->getTran() : Language::getInstance()->getCurrentLanguage();
		$controller = $request->getController () ? $request->getController() : Conf::getInstance()->defaultController;
		$method = explode("?", $request->getMethod());
		$method = $method[0];
		$args = $request->getArgs ();
			
		Language::getInstance()->setCurrentLanguage($tran);

		$controllerFile = SITE_PATH . 'webpages/' . $webpage . '/Controller/' . $controller . '/' . $controller . '.class.php';
		
		if (is_readable ( $controllerFile )) {
			require_once ($controllerFile);
			
			$controller.= "Controller";
			$controller = new $controller ();			
			$method = (is_callable ( array ($controller,$method) )) ? $method : Conf::getInstance()->defaultMethod;
			
			if (! empty ( $args )) {
				return call_user_func_array ( array (
				$controller,
				$method
				), $args );
			} else {
				return call_user_func ( array (
				$controller,
				$method
				) );
			}
			
			return;
		} 
		
		throw new Exception("page $method not found", 404);
	}
}
?>
