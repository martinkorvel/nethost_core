<?php

class Alias extends ClassAPI
{

    private $_id;

    private $_name;

    private $_webpage;

    private $_controller;

    private $_method;

    private $_args;

    private $_added;
    
    public function getTableName() {
        return Conf::getInstance()->tablePrefix . "alias";
    }

    public function getId()
    {
        return $this->_id;
    }

    public function getName()
    {
        return $this->_name;
    }

    public function getWebpage()
    {
        return $this->_webpage;
    }

    public function getController()
    {
        return $this->_controller;
    }

    public function getMethod()
    {
        return $this->_method;
    }

    public function getArgs()
    {
        return $this->_args;
    }

    public function getAdded()
    {
        return $this->_added;
    }

    public function setId($_id)
    {
        $this->_id = $_id;
    }

    public function setName($_name)
    {
        $this->_name = $_name;
    }

    public function setWebpage($_webpage)
    {
        $this->_webpage = $_webpage;
    }

    public function setController($_controller)
    {
        $this->_controller = $_controller;
    }

    public function setMethod($_method)
    {
        $this->_method = $_method;
    }

    public function setArgs($_args)
    {
        $this->_args = $_args;
    }

    public function setAdded($_added)
    {
        $this->_added = $_added;
    }

    private function _fillData($id=0)
    {
        global $DB;
        
        $result = $DB->query("SELECT * FROM {$this->getTableName()} WHERE id = $id");
        
        if(sizeof($result) > 0) {
            $result = $result[0];
            
            $this->setId($result['id']);
            $this->setName($result['name']);
            $this->setWebpage($result['webpage']);
            $this->setController($result['controller']);
            $this->setMethod($result['method']);
            $this->setArgs($result['args']);
            $this->setAdded($result['added']);
        }
    }

    public static function getAliasByName($name = "")
    {
        global $DB;
                
        $alias = new Alias();
        
        $name = addslashes($name);
        
        $result = $DB->query("SELECT id FROM {$alias->getTableName()} WHERE name = '$name'");
        
        if(sizeof($result) > 0) {
            $alias->_fillData($result[0]['id']);
            
            return $alias;
        }
        
        return null;
    }

    public static function getAliasById($id = 0)
    {
        $alias = new Alias();
        $alias->_fillData($id);
        
        if($alias->getId()) {        
            return $alias;        
        }
        
        return null;
    }
    
    public function delete($id=0) {
        global $DB;
        
        return $DB->delete($this->getTableName(), "id = $id");
    }
    
    public function save() {
        global $DB;
        
        $ins = array();
        
        if($this->getName()) {
            $ins['name'] = addslashes($this->getName());
        }
        
        if($this->getWebpage()) {
            $ins['webpage'] = addslashes($this->getWebpage());
        }
        
        if($this->getController()) {
            $ins['controller'] = addslashes($this->getController());
        }
        
        if($this->getMethod()) {
            $ins['method'] = addslashes($this->getMethod());
        }
        
        if($this->getArgs()) {
            $ins['args'] = addslashes($this->getArgs());
        }
        
        if($this->getId()) {
            $DB->update($this->getTableName(), $ins, "id = {$this->getId()}");    
        } else {
            $DB->insert($this->getTableName(), $ins);
        }
    }
}
?>