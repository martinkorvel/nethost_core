<?php

final class Installer extends ClassAPI
{

    const STRING_VERSION = "version";

    const STRING_VERSION_MAJOR = "major";

    const STRING_VERSION_MINOR = "minor";

    const STRING_VERSION_REVISION = "revision";

    private static $_instance;

    public static function getInstance()
    {
        if (! self::$_instance instanceof self) {
            self::$_instance = new Installer();
        }

        return self::$_instance;
    }

    private $_table = "";

    public function __construct()
    {
        parent::__construct();

        $this->_table = Conf::getInstance()->tablePrefix . self::STRING_VERSION;
    }

    public function getInstalledVersion($asSql = false)
    {
        global $DB;

        $version = $DB->query("SELECT * FROM {$this->_table} order by id desc limit 1", false);

        if ($version) {
            $versionData = $version[0];
            $version = $versionData[self::STRING_VERSION];
            $major = $versionData[self::STRING_VERSION_MAJOR];
            $minor = $versionData[self::STRING_VERSION_MINOR];
            $revision = $versionData[self::STRING_VERSION_REVISION];
            if ($asSql) {
                return $version . "." . $major . "." . $minor . "." . $revision . ".sql";
            } else {
                return $version . "." . $major . "." . $minor . "." . $revision;
            }
        }

        return 0;
    }

    private function _checkIfVersionInstalled($version)
    {
        global $DB;

        $parts = explode(".", $version);

        $ins = array();

        $ins[self::STRING_VERSION] = $parts[0];
        $ins[self::STRING_VERSION_MAJOR] = $parts[1];
        $ins[self::STRING_VERSION_MINOR] = $parts[2];
        $ins[self::STRING_VERSION_REVISION] = $parts[3];

        $res = $DB->query("SELECT 1 FROM {$this->_table} where version = {$ins[self::STRING_VERSION]} and major = {$ins[self::STRING_VERSION_MAJOR]} and minor = {$ins[self::STRING_VERSION_MINOR]} and revision = {$ins[self::STRING_VERSION_REVISION]}", false);
        if ($res) {
            return true;
        }

        return false;
    }

    public function update()
    {
        global $DB;

        $versionPure = $this->getInstalledVersion();
        $version = $versionPure . ".sql";

        $path = SITE_PATH . 'upgrade/SQL/';

        $return = "";

        $files = scandir($path);
        unset($files[0]);
        unset($files[1]);

        if (! $version) {
            foreach ($files as $file) {
                if (is_readable($path . $file)) {
                    $handle = fopen($path . $file, "r");
                    if ($handle) {
                        while (($line = fgets($handle)) !== false) {
                            $DB->_dbSql($line);
                        }

                        fclose($handle);
                    } else {
                        // error opening the file.
                    }
                    $return .= "SYSYEM SQL installed " . $file . "<br>";
                    $parts = explode(".", $file);

                    $ins = array();

                    $ins[self::STRING_VERSION] = $parts[0];
                    $ins[self::STRING_VERSION_MAJOR] = $parts[1];
                    $ins[self::STRING_VERSION_MINOR] = $parts[2];
                    $ins[self::STRING_VERSION_REVISION] = $parts[3];
                    $DB->insert($this->_table, $ins);
                }
            }
        } else {
            foreach ($files as $file) {
                if (is_readable($path . $file) && version_compare($versionPure, str_replace(".sql", "", $file), "<") && ! $this->_checkIfVersionInstalled($file)) {
                    $handle = fopen($path . $file, "r");
                    if ($handle) {
                        while (($line = fgets($handle)) !== false) {
                            $DB->_dbSql($line);
                        }

                        fclose($handle);
                    } else {
                        // error opening the file.
                    }
                    $return .= "SYSTEM SQL installed " . $file . "<br>";
                    $parts = explode(".", $file);
                    $ins[self::STRING_VERSION] = $parts[0];
                    $ins[self::STRING_VERSION_MAJOR] = $parts[1];
                    $ins[self::STRING_VERSION_MINOR] = $parts[2];
                    $ins[self::STRING_VERSION_REVISION] = $parts[3];
                    $DB->insert($this->_table, $ins);
                }
            }
        }

        return $return;
    }
}