<?php
/**
 * 
 * Klass, mis manipuleerib linkidega
 * @author Martin
 *
 */
final class Link extends ClassAPI{
	private static $_instance;
	
	public static function getInstance(){
		if(!self::$_instance instanceof self){
			self::$_instance = new Link();
		}
		
		return self::$_instance;
	}
	
	public function createFullUrl($webpage = null, $tran = null, $controller = null, $method = null, $args = null){
		if(!$controller) {
			return SERVER_NAME;
		}
		$url = SERVER_NAME . $webpage . '/' . $tran . '/' . $controller . '/' . $method;
		if($args){
			foreach($args as $arg){
				$url.='/' . $arg;
			}
		}
		
		return $url;
	}
	
	public function createUrl($controller = null, $method = null, $args = null){
		global $CURRENTWEB, $LANG;
		
		if(!$controller) {
			return SERVER_NAME;
		}
		$url = SERVER_NAME . $CURRENTWEB . '/' . $LANG . '/' . $controller . '/' . $method;
		if($args){
			foreach($args as $arg){
				$url.='/' . $arg;
			}
		}
		
		return $url;
	}
	
	public function createAliasUrl($alias, $args=null){
	    global $CURRENTWEB, $LANG;
	    
	    if(!$alias) {
	        return SERVER_NAME;
	    }
	    $url = SERVER_NAME . $alias . '/' . $LANG;
	    if($args){
	        foreach($args as $arg){
	            $url.='/' . $arg;
	        }
	    }
	    
	    return $url;
	}
	
	public function currentURL() {
	    if(Conf::getInstance()->enableHttps){
			$pageURL = 'https';
		} else {
			$pageURL = 'http';
		}
		
		$pageURL .= "://";
		if ($_SERVER["SERVER_PORT"] != "80") {
			$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		} else {
			$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		}
		
		return $pageURL;
	}
	
	public function getFilesDir(){
		return SERVER_NAME ."files/";
	}
}