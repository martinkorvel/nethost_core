<?php

class File extends ClassAPI
{

    public function __construct($id = 0)
    {
        global $DB;

        if ($id) {
            $res = $DB->query("SELECT * FROM " . self::getTableName() . " WHERE id = $id");
            
            if ($res) {
                $res = $res[0];
                $this->setId($res['id']);
                $this->setName($res['name']);
                $this->setType($res['type']);
                $this->setSize($res['size']);
                $this->setontent($res['content']);
            }
        }
        parent::__construct();
    }

    private static function getTableName()
    {
        return Conf::getInstance()->tablePrefix . "file";
    }

    private $_id;

    private $_name;

    private $_type;

    private $_size;

    private $_content;

    public function setId($id)
    {
        $id = (int) $id;
        $this->_id = $id;
    }

    public function getId()
    {
        return $this->_id;
    }

    public function setName($name)
    {
        $this->_name = $name;
    }

    public function getName()
    {
        return $this->_name;
    }

    public function setType($type)
    {
        $this->_type = $type;
    }

    public function getType()
    {
        return $this->_type;
    }

    public function setSize($size)
    {
        $this->_size = $size;
    }

    public function getSize()
    {
        return $this->_size;
    }

    public function setContent($content)
    {
        $this->_content = $content;
    }

    public function getContent()
    {
        return $this->_content;
    }

    public function save()
    {
        global $DB;

        $data = array();

        $data['name'] = addslashes($this->getName());
        $data['type'] = addslashes($this->getType());
        $data['size'] = addslashes($this->getSize());
        $data['content'] = addslashes($this->getContent());

        if ($this->getId()) {
            $DB->update(self::getTableName(), $data, "id = " . $this->getId());
        } else {
            $id = $DB->insert(self::getTableName(), $data);
            $this->setId($id);
        }
    }

    public function delete()
    {
        global $DB;

        if ($this->getId()) {
            $DB->delete(self::getTableName(), "id = " . $this->getId());
        }
    }

    public static function getAllFiles($dir = "files")
    {
        $path = SITE_PATH . $dir;

        $files = scandir($path);

        $parsedFiles = array();

        foreach ($files as $file) {

            if ($file != "index.php" && strlen($file) > 2) {
                $parsedFiles[] = $file;
            }
        }

        return $parsedFiles;
    }
}
?>