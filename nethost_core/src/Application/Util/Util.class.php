<?php

class Util extends ClassAPI
{

    public static function currentTimestamp()
    {
        return date("Y-m-d H:i:s");
    }

    public static function go($link)
    {
        header("Location: $link");
    }

    public static function myAddslashes($sisu)
    {
        return "'" . addslashes($sisu) . "'";
    }

    public static function uid($function)
    {
        return substr(md5($function . microtime()), 0, 6);
    }

    public static function hash($function)
    {
        return substr(md5($function . microtime()), 0, 16);
    }

    public static function getPassword($input)
    {
        return md5(sha1($input));
    }

    public static function getAllDirNames($path)
    {
        return array_diff(scandir($path), array(
            '..',
            '.'
        ));
    }

    public static function getAllWebpages()
    {
        $webPages = self::getAllDirNames(SITE_PATH . "webpages");
        $newArray = array();
        $i = 0;
        foreach ($webPages as $wp) {
            $newArray[$i] = $wp;
            $i ++;
        }

        return $newArray;
    }

    public static function getAllCrons()
    {
        $webPages = self::getAllDirNames(SITE_PATH . "src/Cron");
        $newArray = array();
        $i = 0;
        foreach ($webPages as $wp) {
            if ($wp != "nethost") {
                $newArray[$i] = $wp;
                $i ++;
            }
        }

        return $newArray;
    }

    public static function getGenderByIsikukood($isikukood)
    {
        $naine = array(
            2,
            4,
            6,
            8
        );

        if (in_array(substr($isikukood, 0, 1), $naine)) {
            return "F";
        }

        return "M";
    }

    public static function bytesToMB($size, $precision = 0)
    {
        $base = log($size, 1024);
        $suffixes = array(
            '',
            'K',
            'M',
            'G',
            'T'
        );

        return round(pow(1024, $base - floor($base)), $precision) . ' ' . $suffixes[floor($base)];
    }

    public static function getServerIp()
    {
        return $_SERVER['SERVER_ADDR'];
    }

    public static function returnType($input)
    {
        $returnType = "STRING";

        if (is_numeric($input)) {
            if (strstr($input, ".") && floatval($input)) {
                $returnType = "FLOAT";
            }

            if (intval($input)) {
                $returnType = "INT";
            }
        }
        if (strlen($input) <= 0) {
            $returnType = "EMPTY";
        }

        return $returnType;
    }
}
