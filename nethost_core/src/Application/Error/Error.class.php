<?php
class Error extends ClassAPI{
	public static function compute(Exception $ex, $includeStackTrace = true){
		global $SCREEN;

		$SCREEN->add('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">');
		$SCREEN->add('<html>');
		$SCREEN->add('<head>');
		$SCREEN->add('<title>Error ' . $ex->getCode() . '</title>');
		$SCREEN->add('</head>');
		$SCREEN->add('<body>');
		$SCREEN->add('<h2>');
		$SCREEN->add('Error ' . $ex->getCode() . ' ' . $ex->getMessage());
		$SCREEN->add('</h2>');
		if($includeStackTrace){
			$SCREEN->add($ex->getTraceAsString());
		}
		$SCREEN->add('</body>');
		$SCREEN->add('</html>');

		return;
	}
}
?>