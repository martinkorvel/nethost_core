<?php

/**
 *
 * Loeb ja parseb URL-i
 * @author Martin
 *
 */
class Request extends ClassAPI
{

    private $_webpage;

    private $_tran;

    private $_controller;

    private $_method;

    private $_args;

    public function setWebpage($_webpage)
    {
        $this->_webpage = $_webpage;
    }

    public function setController($_controller)
    {
        $this->_controller = $_controller;
    }

    public function setMethod($_method)
    {
        $this->_method = $_method;
    }

    public function setArgs($_args)
    {
        $this->_args = $_args;
    }

    public function __construct()
    {
        $parts = explode('/', $_SERVER['REQUEST_URI']);
        if (sizeof(Conf::getInstance()->subpages) > 0) {
            foreach (Conf::getInstance()->subpages as $subUrl) {
                $key = array_search($subUrl, $parts);
                if ($key) {
                    for ($i = 0; $i <= $key; $i ++) {
                        unset($parts[$i]);
                    }
                }
            }
        }

        if ($parts[0] = ' ') {
            unset($parts[0]);
        }

        if (isset($parts[1]) && substr($parts[1], 0, 1) == "?") {
            unset($parts[1]);
        }

        $this->_webpage = ($c = array_shift($parts)) ? $c : '';
        $this->_tran = ($c = array_shift($parts)) ? $c : '';
        $this->_controller = ($c = array_shift($parts)) ? $c : '';
        $this->_method = ($c = array_shift($parts)) ? $c : '';
        $this->_args = (isset($parts[0])) ? $parts : array();
    }

    public function getWebpage()
    {
        return $this->_webpage;
    }

    public function getTran()
    {
        return $this->_tran;
    }

    public function getController()
    {
        return $this->_controller;
    }

    public function getMethod()
    {
        return $this->_method;
    }

    public function getArgs()
    {
        return $this->_args;
    }

    public function setTran($tran)
    {
        $this->_tran = $tran;
    }
}
?>