<?php
final class Translator extends ClassAPI {
	private static $_instance;
	
	public static function getInstance() {
		if (! self::$_instance instanceof self) {
			self::$_instance = new Translator ();
		}
		return self::$_instance;
	}
	
	private $_table = "";
	private $_cache = array ();
	
	public function __construct() {
		global $DB;
		
		parent::__construct ();
		
		$this->_table = Conf::getInstance ()->tablePrefix . 'translate';
		
		$currentLang = Language::getInstance ()->getCurrentLanguage ();
		
		$res = $DB->query ( "SELECT phrase, tran FROM {$this->_table} WHERE lang = '$currentLang'", false );
		
		if (is_array ( $res )) {			
			foreach ( $res as $item ) {
				$this->_cache [md5 ( addslashes ( $item ['phrase'] ) )] = $item ['tran'];
			}
		}
	}
	
	public function translate($phrase) {
		global $DB;
		
		$currentLang = Language::getInstance ()->getCurrentLanguage ();
		$phrase = addslashes ( $phrase );
		
		if (array_key_exists ( md5 ( $phrase ), $this->_cache )) {
			return $this->_cache [md5 ( $phrase )];
		}
		
		$res = $DB->query ( "SELECT tran FROM {$this->_table} WHERE lang = '$currentLang' AND phrase = '$phrase'" );
		
		if($res){
			return $phrase;
		}
		
		$DB->insert ( $this->_table, array (
				'phrase' => $phrase,
				'tran' => $phrase,
				'lang' => $currentLang 
		) );
		
		return $phrase;
	}
}