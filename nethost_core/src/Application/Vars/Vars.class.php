<?php
final class Vars extends ClassAPI{
	
	private $_registry;
	
	public function __construct(){
		if($_GET){
			foreach ($_GET as $k=>$i){
				$this->_registry[$k] = $i;
			}
		}
		if($_POST){
			foreach ($_POST as $k=>$i){
				$this->_registry[$k] = $i;
			}
		}
		if($_FILES){
			foreach ($_FILES as $k=>$i){
				$this->_registry[$k] = $i;
			}
		}
		if($_REQUEST){
			foreach ($_REQUEST as $k=>$i){
				$this->_registry[$k] = $i;
			}
		}
	}
	
	public function get($key){		
		if(isset($this->_registry[$key])){
			return $this->_registry[$key];
		}
		
		return null;
	}	
}