<?php

/**
 * 
 * @author Martin
 *
 */
class DefaultRest extends RestAPI
{

    public function __construct()
    {
        parent::__construct();
    }

    public function test($sisend)
    {
        return self::success(array(
            "response" => array(
                "aaa" => "TEST REST SUCCESS"
            ),
            "request" => $sisend
        ));
    }
}
