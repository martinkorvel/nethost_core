<?php 
interface Idatabase {
    public function __construct($host, $user, $pass, $dbName, $port);
    public function _connect();
    public function _disconnect();    
    public function _dbSql($sql);    
    public function query($qry, $showErrors = true);    
    public function checkIfDatabaseExist($database);
    public function checkIfTableExists($table);    
}
?>