<?php

// new autoload
function listFolderFiles($dir)
{
    $fileInfo = scandir($dir);
    $allFileLists = [];
    
    foreach ($fileInfo as $folder) {
        if ($folder !== '.' && $folder !== '..') {
            if (is_dir($dir . DIRECTORY_SEPARATOR . $folder) === true) {
                $allFileLists[$folder . '/'] = listFolderFiles($dir . DIRECTORY_SEPARATOR . $folder);
            } else {
                $allFileLists[$folder] = $folder;
            }
        }
    }
    
    return $allFileLists;
}
// interface
foreach (listFolderFiles(SRC_PATH . 'Interface/') as $i) {
    foreach (array_keys($i) as $j) {       
        $x = explode(".", $j);
        if (file_exists(SRC_PATH . 'Interface/' . $x[0] . '/')) {
            if ($handle = opendir(SRC_PATH . 'Interface/' . $x[0] . '/')) {                
                while (false !== ($entry = readdir($handle))) {                   
                    $jupid = explode('.', $entry);
                    if ($jupid[1] == "interface") {
                        // autoload
                        $tee = SRC_PATH . 'Interface/' . $x[0] . '/' . $jupid[0] . '.interface.php';
                        if (is_readable($tee)) {
                            require_once $tee;
                        }
                    }
                }
                closedir($handle);
            }
        }
    }
}

// api
if (is_readable(SRC_PATH . 'API/MainAPI.class.php')) {
    require_once SRC_PATH . 'API/MainAPI.class.php';
}

if (is_readable(SRC_PATH . 'API/ClassAPI.class.php')) {
    require_once SRC_PATH . 'API/ClassAPI.class.php';
}

if (is_readable(SRC_PATH . 'API/ControllerAPI.class.php')) {
    require_once SRC_PATH . 'API/ControllerAPI.class.php';
}

if (is_readable(SRC_PATH . 'API/ModelAPI.class.php')) {
    require_once SRC_PATH . 'API/ModelAPI.class.php';
}

if (is_readable(SRC_PATH . 'API/PluginAPI.class.php')) {
    require_once SRC_PATH . 'API/PluginAPI.class.php';
}

if (is_readable(SRC_PATH . 'API/SessionAPI.class.php')) {
    require_once SRC_PATH . 'API/SessionAPI.class.php';
}

if (is_readable(SRC_PATH . 'API/DatabaseAPI.class.php')) {
    require_once SRC_PATH . 'API/DatabaseAPI.class.php';
}

if (is_readable(SRC_PATH . 'API/CronAPI.class.php')) {
    require_once SRC_PATH . 'API/CronAPI.class.php';
}

if (is_readable(SRC_PATH . 'API/WebsocketsAPI.class.php')) {
    require_once SRC_PATH . 'API/WebsocketsAPI.class.php';
}

if (is_readable(SRC_PATH . 'API/RestAPI.class.php')) {
    require_once SRC_PATH . 'API/RestAPI.class.php';
}
// more
/**
 * AUTOLOAD START
 */

$autoLoaderClass = Conf::getInstance()->autoLoaderClass;
$autoLoaderFunctions = Conf::getInstance()->autoLoaderFunctions;

if (is_readable(SRC_PATH . $autoLoaderClass . '/' . $autoLoaderClass . '.class.php')) {
    require_once SRC_PATH . $autoLoaderClass . '/' . $autoLoaderClass . '.class.php';
    
    foreach ($autoLoaderFunctions as $loaderFunction) {
        if (is_callable($autoLoaderClass . "::" . $loaderFunction)) {
            spl_autoload_register($autoLoaderClass . "::" . $loaderFunction);
        } else {
            trigger_error($autoLoaderClass . "::" . $loaderFunction . ' not callable', E_USER_ERROR);
        }
    }
} else {
    trigger_error("class " . $autoLoaderClass . ".class.php" . " not found", E_USER_ERROR);
    exit();
}

if (is_array(Conf::getInstance()->customRequired)) {
    foreach (Conf::getInstance()->customRequired as $i) {
        if (is_readable(SITE_PATH . $i)) {
            require_once SITE_PATH . $i;
        }
    }
}

require_once SITE_PATH . 'lib/phpmailer/class.phpmailer.php';
require_once SITE_PATH . 'lib/Smarty/libs/Smarty.class.php';

/**
 * AUTOLOAD END
 */

?>