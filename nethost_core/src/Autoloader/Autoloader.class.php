<?php
class Autoloader{
	protected static $_instance;
	
	public static function getInstance() {
		if (! isset ( self::$instance )) {
			$c = __CLASS__;
			self::$_instance = new $c ();
		}

		return self::$_instance;
	}

	public static function ApplicationLoader($className) {
		if (file_exists ( SRC_PATH . 'Application/' . $className . '/' )) {
			if ($handle = opendir ( SRC_PATH . 'Application/' . $className . '/' )) {
				while ( false !== ($entry = readdir ( $handle )) ) {
					$jupid = explode ( '.', $entry );
					if ($jupid [1] == "class") {
						// autoload
						$tee = SRC_PATH . 'Application/' . $className . '/' . $jupid [0] . '.class.php';
						if (is_readable ( $tee )) {							
							require_once $tee;
						}
					}
				}
				closedir ( $handle );
			}
		}
	}

	public static function ModelLoader($className) {
		$className = str_replace("Model", "", $className);		
		if (file_exists ( SRC_PATH . 'Model/' . $className . '/' )) {
			if ($handle = opendir ( SRC_PATH . 'Model/' . $className . '/' )) {
				while ( false !== ($entry = readdir ( $handle )) ) {
					$jupid = explode ( '.', $entry );
					if ($jupid [1] == "model") {
						// autoload
						$tee = SRC_PATH . 'Model/' . $className . '/' . $jupid [0] . '.model.php';
						if (is_readable ( $tee )) {							
							require_once $tee;
						}
					}
				}
				closedir ( $handle );
			}
		}
	}
	
	public static function CronLoader($className) {
		$className = str_replace("Cron", "", $className);
		if (file_exists ( SRC_PATH . 'Cron/' . $className . '/' )) {
			if ($handle = opendir ( SRC_PATH . 'Cron/' . $className . '/' )) {
				while ( false !== ($entry = readdir ( $handle )) ) {
					$jupid = explode ( '.', $entry );
					if ($jupid [1] == "cron") {
						// autoload
						$tee = SRC_PATH . 'Cron/' . $className . '/' . $jupid [0] . '.cron.php';
						if (is_readable ( $tee )) {
							require_once $tee;
						}
					}
				}
				closedir ( $handle );
			}
		}
	}
	
	public static function WebsocketsLoader($className) {
	    $className = str_replace("Websockets", "", $className);
	    if (file_exists ( SRC_PATH . 'Websockets/' . $className . '/' )) {
	        if ($handle = opendir ( SRC_PATH . 'Websockets/' . $className . '/' )) {
	            while ( false !== ($entry = readdir ( $handle )) ) {
	                $jupid = explode ( '.', $entry );
	                if ($jupid [1] == "websockets") {
	                    // autoload
	                    $tee = SRC_PATH . 'Websockets/' . $className . '/' . $jupid [0] . '.websockets.php';
	                    if (is_readable ( $tee )) {
	                        require_once $tee;
	                    }
	                }
	            }
	            closedir ( $handle );
	        }
	    }
	}
	
	public static function PluginLoader($className) {
		$className = str_replace("Plugin", "", $className);
		if (file_exists ( SRC_PATH . 'Plugin/' . $className . '/' )) {
			if ($handle = opendir ( SRC_PATH . 'Plugin/' . $className . '/' )) {
				while ( false !== ($entry = readdir ( $handle )) ) {
					$jupid = explode ( '.', $entry );
					if ($jupid [1] == "plugin") {
						// autoload
						$tee = SRC_PATH . 'Plugin/' . $className . '/' . $jupid [0] . '.plugin.php';
						if (is_readable ( $tee )) {
							require_once $tee;
						}
					}
				}
				closedir ( $handle );
			}
		}
	}	
	
	public static function RestLoader($className) {
	    $className = str_replace("Rest", "", $className);
	    if (file_exists ( SRC_PATH . 'Rest/' . $className . '/' )) {
	        if ($handle = opendir ( SRC_PATH . 'Rest/' . $className . '/' )) {
	            while ( false !== ($entry = readdir ( $handle )) ) {
	                $jupid = explode ( '.', $entry );
	                if ($jupid [1] == "rest") {
	                    // autoload
	                    $tee = SRC_PATH . 'Rest/' . $className . '/' . $jupid [0] . '.rest.php';
	                    if (is_readable ( $tee )) {
	                        require_once $tee;
	                    }
	                }
	            }
	            closedir ( $handle );
	        }
	    }
	}	
}
?>