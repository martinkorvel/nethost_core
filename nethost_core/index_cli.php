<?php
$startTime = microtime ( true );

error_reporting ( E_ALL );
session_start ();

define ( 'SITE_PATH', realpath ( dirname ( __FILE__ ) ) . '/' );
define ( 'SRC_PATH', SITE_PATH . 'src/' );

if (is_readable ( SITE_PATH . 'conf.php' )) {
	require_once SITE_PATH . 'conf.php';
} else {
	trigger_error ( SITE_PATH . 'conf.php' . ' not found', E_USER_ERROR );
}

if (is_readable ( SITE_PATH . 'lib/websockets/websockets.php' )) {
    require_once SITE_PATH . 'lib/websockets/websockets.php';
} else {
    trigger_error ( SITE_PATH . 'websockets.php' . ' not found', E_USER_ERROR );
}

$VER = file_get_contents(SITE_PATH . 'version.php');

if (is_readable ( SITE_PATH . 'src/Autoloader/Autoload.php' )) {
    require_once SITE_PATH . 'src/Autoloader/Autoload.php';
} else {
    trigger_error ( SITE_PATH . 'autoload.php' . ' not found', E_USER_ERROR );
}

$CURRENTWEB = '';

$S = new Smarty ();
$S->config_dir = SITE_PATH . 'temp/Smarty/config';
$S->cache_dir = SITE_PATH . 'temp/Smarty/cache';
$S->compile_dir = SITE_PATH . 'temp/Smarty/compile';
$S->error_reporting = E_ALL & ~ E_NOTICE;

$SCREEN = new Screen ();
$SCREEN->clearBuffer ();

$VARS = new Vars ();

if (Conf::getInstance ()->useDB) {
    $DB = new MySqliConnector ( Conf::getInstance ()->dbHost, Conf::getInstance ()->dbUser, Conf::getInstance ()->dbPassword, Conf::getInstance ()->dbDatabase, Conf::getInstance ()->dbPort );
}

$LOG = Log::getInstance ();

$LANG = Language::getInstance ()->getCurrentLanguage ();

if (Conf::getInstance()->enableHttps) {
	if (key_exists ( 'HTTPS', $_SERVER )) {
		if ($_SERVER ["HTTPS"] != "on") {
			Util::go ( Link::getInstance ()->currentURL () );
		}
	}
}

try {
	if (Conf::getInstance ()->useDB) {
		$TRANSLATOR = Translator::getInstance ();
		
		// *********** INSTALL / UPDATE *************//
		$installer = Installer::getInstance ()->update ();
		
		if ($installer) {
			$SCREEN->add ( $installer );
			$SCREEN->render ();
			$SCREEN->clearBuffer ();
			exit ();
		}
		
		$installerClient = InstallerClient::getInstance ()->update ();
		
		if ($installerClient) {
			$SCREEN->add ( $installerClient );
			$SCREEN->render ();
			$SCREEN->clearBuffer ();
			exit ();
		}
	}
	//CLI START
	echo "Cli finished. System version: " . $VER  . " Database version " . Installer::getInstance()->getInstalledVersion();
	//CLI END
} catch ( Exception $e ) {
	if (Conf::getInstance ()->debug) {
		Error::compute ( $e );
		$SCREEN->render ();
	}
	exit ();
}
?>