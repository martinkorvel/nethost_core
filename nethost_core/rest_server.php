<?php
$startTime = microtime ( true );

error_reporting ( E_ALL );

session_start ();

define ( 'SITE_PATH', realpath ( dirname ( __FILE__ ) ) . '/' );
define ( 'SRC_PATH', SITE_PATH . 'src/' );

if (is_readable ( SITE_PATH . 'conf.php' )) {
    require_once SITE_PATH . 'conf.php';
} else {
    trigger_error ( SITE_PATH . 'conf.php' . ' not found', E_USER_ERROR );
}

if (is_readable ( SITE_PATH . 'lib/websockets/websockets.php' )) {
    require_once SITE_PATH . 'lib/websockets/websockets.php';
} else {
    trigger_error ( SITE_PATH . 'websockets.php' . ' not found', E_USER_ERROR );
}

$VER = file_get_contents(SITE_PATH . 'version.php');

$subpagesArray = Conf::getInstance()->subpages;

$subpages = implode ( '/', $subpagesArray );

if (sizeof ( $subpagesArray ) > 0) {
    $subpages .= '/';
}

$proto = Conf::getInstance()->enableHttps ? 'https://' : 'http://';

if (is_readable ( SITE_PATH . 'src/Autoloader/Autoload.php' )) {
    require_once SITE_PATH . 'src/Autoloader/Autoload.php';
} else {
    trigger_error ( SITE_PATH . 'autoload.php' . ' not found', E_USER_ERROR );
}

if (Conf::getInstance ()->useDB) {
    $DB = new MySqliConnector ( Conf::getInstance ()->dbHost, Conf::getInstance ()->dbUser, Conf::getInstance ()->dbPassword, Conf::getInstance ()->dbDatabase, Conf::getInstance ()->dbPort );
}

$LOG = Log::getInstance ();

error_reporting(E_ALL & ~(E_STRICT|E_NOTICE));
try {
   //REST SERVER START
  $rest = $_REQUEST['rest'];
  $rest = json_decode($HTTP_RAW_POST_DATA, true);

  if(!$rest){
      throw new Exception("unparsable request");
  }
  
  if(!key_exists("uid", $rest)){
      throw new Exception("UID missing");
  }
  
  if(!key_exists("object", $rest)){
      throw new Exception("Object missing");
  }
  
  if(!key_exists("method", $rest)){
      throw new Exception("Method missing");
  }
  
  $uid = $rest['uid'];
  $object = $rest['object'];
  $method = $rest['method'];
  
  $data = array();
  
  if(key_exists("data", $rest)){
      $data = $rest['data'];
  }  
  
  //uid kontroll kas võib resti teha või mitte
  
  
  
  if(!class_exists($object . "Rest") ) {
      throw new Exception("Unknown object " . $object);
  }
  
  if(!method_exists($object . "Rest", $method)) {
      throw new Exception("Unknown method " . $method);
  }
  
  $class = $object . "Rest";
  
  $class = new $class();
  
  $result = call_user_func_array(array($object . "Rest", $method), array($data));
  
  echo json_encode($result);
  
  exit;
   
} catch ( Exception $e ) {
    
    echo json_encode(array('success' => false, 'data' => array('error' => $e->getMessage())));
    exit ();
}
?>
