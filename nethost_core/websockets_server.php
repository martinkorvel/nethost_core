<?php
$startTime = microtime ( true );

error_reporting ( E_ALL );

set_time_limit (0);

session_start ();

define ( 'SITE_PATH', realpath ( dirname ( __FILE__ ) ) . '/' );
define ( 'SRC_PATH', SITE_PATH . 'src/' );

if (is_readable ( SITE_PATH . 'conf.php' )) {
    require_once SITE_PATH . 'conf.php';
} else {
    trigger_error ( SITE_PATH . 'conf.php' . ' not found', E_USER_ERROR );
}

if (is_readable ( SITE_PATH . 'lib/websockets/websockets.php' )) {
    require_once SITE_PATH . 'lib/websockets/websockets.php';
} else {
    trigger_error ( SITE_PATH . 'websockets.php' . ' not found', E_USER_ERROR );
}

$VER = file_get_contents(SITE_PATH . 'version.php');

$subpagesArray = Conf::getInstance()->subpages;
$subpages = implode ( '/', $subpagesArray );

if (sizeof ( $subpagesArray ) > 0) {
    $subpages .= '/';
}

$proto = Conf::getInstance()->enableHttps ? 'https://' : 'http://';

if (is_readable ( SITE_PATH . 'src/Autoloader/Autoload.php' )) {
    require_once SITE_PATH . 'src/Autoloader/Autoload.php';
} else {
    trigger_error ( SITE_PATH . 'autoload.php' . ' not found', E_USER_ERROR );
}

if (Conf::getInstance ()->useDB) {
    $DB = new MySqliConnector ( Conf::getInstance ()->dbHost, Conf::getInstance ()->dbUser, Conf::getInstance ()->dbPassword, Conf::getInstance ()->dbDatabase, Conf::getInstance ()->dbPort );
}

$LOG = Log::getInstance ();

?>